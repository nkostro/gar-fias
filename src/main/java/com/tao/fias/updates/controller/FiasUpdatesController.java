package com.tao.fias.updates.controller;

import com.tao.fias.utils.Utils;
import com.tao.fias.xml.XmlConverter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.json.JSONObject;
import org.xml.sax.SAXException;

public class FiasUpdatesController {

    private static final Logger logger = Logger.getGlobal();
    private static final String PROPERTIES_FILE_NAME = "application.properties";

    private final Properties properties = new Properties();
    private final int CONNECT_TIMEOUT;

    public FiasUpdatesController() throws FileNotFoundException, IOException {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)) {
            properties.load(inputStream);
            CONNECT_TIMEOUT = Integer.valueOf(properties.getProperty("timeout"));
        }
    }

    private String downloadUpdatesFile() throws IOException, MalformedURLException {
        JSONObject json = getGetDownloadFileInfo(properties.getProperty("download.file.url"));
        String garXmlDeltaUrl = json.getString(properties.getProperty("gar.xml.delta.url.field"));
        String date = json.getString("Date");
        String garXmlDeltaFileName = date + "_" + garXmlDeltaUrl.substring(garXmlDeltaUrl.lastIndexOf("/") + 1);

        logger.log(Level.INFO, "GAR XML File Url: {0}", garXmlDeltaUrl);

        URL connectionUrl = new URL(garXmlDeltaUrl);

        try ( BufferedInputStream input = new BufferedInputStream(connectionUrl.openStream())) {
            Files.copy(input, Paths.get(garXmlDeltaFileName), StandardCopyOption.REPLACE_EXISTING);
        }

        return garXmlDeltaFileName;
    }

    private JSONObject getGetDownloadFileInfo(String urlName) throws IOException, MalformedURLException, RuntimeException {
        logger.log(Level.INFO, "Download File Url: {0}", urlName);
        URL fiasUrl = new URL(urlName);

        HttpURLConnection fiasConnection = (HttpURLConnection) fiasUrl.openConnection();
        fiasConnection.setRequestProperty("Accept", "application/json");
        fiasConnection.setReadTimeout(CONNECT_TIMEOUT);
        fiasConnection.setConnectTimeout(CONNECT_TIMEOUT);

        int responseCode = fiasConnection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Response code: " + responseCode);
        }

        StringBuilder sb = new StringBuilder();
        try ( Scanner scanner = new Scanner(fiasConnection.getInputStream())) {
            while (scanner.hasNext()) {
                sb.append(scanner.next());
            }
        }
        fiasConnection.disconnect();

        return new JSONObject(sb.toString());
    }

    private void convertToCsv(File sourceDir) throws SAXException, IOException,
            ParserConfigurationException, TransformerConfigurationException, TransformerException {
        String xmlSource = properties.getProperty("xml.source");
        Path xmlSourcePath = Paths.get(sourceDir.getCanonicalPath(), xmlSource);
        String xmlStylesheet = getClass().getClassLoader().getResource(properties.getProperty("xslt.source")).getFile();
        String outputCsv = xmlSourcePath.getFileName().toString();
        outputCsv = outputCsv.substring(0, outputCsv.lastIndexOf(".")) + ".csv";

        logger.log(Level.INFO, "xml source = {0}, xslt = {1}, output csv = {2}",
                new Object[] {xmlSourcePath.toString(), xmlStylesheet, outputCsv});

        XmlConverter xmlController = new XmlConverter(xmlSourcePath.toString());
        xmlController.convert(xmlStylesheet, new File(outputCsv));
    }

    public static void main(String[] args) throws Exception {
        FiasUpdatesController controller = new FiasUpdatesController();
        String updatesFile = controller.downloadUpdatesFile();
        File destDir = new File(updatesFile.substring(0, updatesFile.lastIndexOf(".")));
        Utils.unzip(updatesFile, destDir);
        controller.convertToCsv(destDir);
    }
}
