<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>

    <xsl:template match="/HOUSES">
        <xsl:text>id, objectId, objectGuid, changeId, houseNum, houseType, operTypeId, prevId, nextId, updateDate, startDate, endDate, isActual, isActive&#10;</xsl:text>
        <xsl:for-each select="//HOUSE">
            <xsl:value-of select="concat(@ID, ',', @OBJECTID, ',', @OBJECTGUID, ',', @CHANGEID, ',', @HOUSENUM, ',', @HOUSETYPE, ',', @OPERTYPEID, ',', @PREVID, ',', @NEXTID, ',', @UPDATEDATE, ',', @STARTDATE, ',', @ENDDATE, ',', @ISACTUAL, ',', @ISACTIVE)"/>
            <xsl:text>&#10;</xsl:text>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
