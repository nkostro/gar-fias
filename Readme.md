Тестовый проект по взаимодействию с ГАР ФИАС (https://fias.nalog.ru/Updates)

Скачивает обновления БД, разархивирует и конвертирует 1 файл из XML в CSV.\
Для конвертации в CSV используется XSLT (resources/csv.xsl)\
Конфигурация находится в resources/application.properties
